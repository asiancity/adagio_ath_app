<?php
/**
 * @package ath_app
 * @version 1.0
 */
/*
Plugin Name: ath Plugins Struktur
Description: Das ist selbst programmiert und ist nur extra für Websites geeignet. Strukture ist eindeutig. Code & Design Folder. In jedem Code Ordner sind verschiedene Plugins, die mit Design Ordner verbunden sind.
Author: Anh Tuan Hoang
*/
define('ATH_PATH', plugin_dir_url(__FILE__) );
require('code/admin/admin.php');
require('code/theme/theme.php');

add_action( 'admin_menu', 'register_my_message_menu_page' );
function register_my_message_menu_page() {

	add_menu_page( 'Eingehende Nachrichten', 'Eingehende Nachrichten', 'read', 'admin.php?page=flamingo_inbound', '', '', 6 );

}
?>
