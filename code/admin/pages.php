<?php
$post_type = 'page';
add_action( 'pre_get_posts', $post_type.'_orderby' );
function page_orderby( $query ) {

    $orderby = $query->get( 'orderby');

    if( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'page') {
        $query->set('orderby','menu_order');
		$query->set('order','asc');
    }
	return $query;
}
?>
