<?php
$post_type = 'ticketkategorie';
add_action( 'pre_get_posts', $post_type.'_orderby' );
add_filter( 'manage_edit-'.$post_type.'_sortable_columns', $post_type.'_sortable_column' );
add_filter( 'manage_edit-'.$post_type.'_columns', $post_type.'_edit_columns' );
add_action( 'manage_posts_custom_column', $post_type. '_column_display', 10, 2 );

function ticketkategorie_orderby( $query ) {

    $orderby = $query->get( 'orderby');

    if( isset($_GET['post_type']) && $_GET['post_type'] == 'ticketkategorie') {
        $query->set('orderby','menu_order');
		$query->set('order','asc');
    }
	return $query;
}
function ticketkategorie_edit_columns( $columns ) {
	$columns = array(
		"cb" => "<input type=\"checkbox\" />",
		"title" => __('Title' ,'rocknrolla'),
		"undertitle" => __('Untertitel', 'rocknrolla'),
		"ticketzusatz" => __('Verfügbarkeit', 'rocknrolla'),
		"price" => __('Preis', 'rocknrolla'),
		"ticket_featured" => __('hervorheben', 'rocknrolla'),
		"orderby" => __('Reihenfolge', 'rocknrolla'),
	);
	return $columns;
}

function ticketkategorie_column_display( $columns, $post_id ) {

	// Code from: http://wpengineer.com/display-post-thumbnail-post-page-overview
	if( $_GET['post_type'] == 'ticketkategorie'){
		$post_meta = get_post_meta( $post_id );
		switch ( $columns ) {

			// Display the thumbnail in the column view
			case "undertitle":
				echo $post_meta['ticket_untertitle'][0];
			break;
			case "price":
				echo $post_meta['ticket_price'][0];
			break;
			case "ticket_featured":
				echo ($post_meta['ticket_featured'][0]) ? 'Ja' : 'Nein';
			break;
			case "ticketzusatz":
				echo $post_meta['ticketzusatz_value'][0];
			break;

			case "orderby":
				echo get_post_field( 'menu_order', $post_id);

			break;
		}
	}
}

function ticketkategorie_sortable_column( $columns ) {
    $columns['orderby'] = 'menu_order';

    return $columns;
}
?>
