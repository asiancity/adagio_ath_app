<?php
add_filter( 'show_admin_bar', '__return_false' );
add_action( 'init', 'theme_custom_style_remove', 10 );
add_action( 'wp_print_styles', 'theme_name_scripts', 15);

// Theme Scripts & Styles
function theme_name_scripts() {
	wp_enqueue_style( 'ath_app-style', ATH_PATH .'assets/css/style.css' );
	wp_deregister_style('countdown_css');
}
// Theme Scripts & Styles
function theme_custom_style_remove () {
    remove_action('wp_head', 'rocknrolla_custom_styles', 100);
}
?>
